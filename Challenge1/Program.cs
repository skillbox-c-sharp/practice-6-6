﻿using System.Globalization;
using System.IO;

namespace Challenge1;
class Program
{
    static void Main(string[] args)
    {
        do
        {
            int workMode = ChooseMode();
            switch (workMode)
            {
                case 1:
                    ShowData();
                    break;
                case 2:
                    string lineToSet = ReadData();
                    SetData(lineToSet);
                    break;
                default:
                    return;
            }
        } while (true);
    }

    static int ChooseMode()
    {
        bool isModeChoosed = false;
        int isWriteData;
        do
        {
            Console.Write("Выберите режим работы (1 - вывести данные, 2 - заполнить данные, 3 - выход): ");
            isWriteData = int.Parse(Console.ReadLine()!);
            if (!(isWriteData > 0 && isWriteData < 4))
            {
                Console.WriteLine("Введите 1, 2 или 3");
            }
            else
            {
                isModeChoosed = true;
            }
        } while (!isModeChoosed);
        return isWriteData;
    }

    static string ReadData()
    {
        string id = SetID().ToString();

        DateTime dateOfAdd = DateTime.Now;
        string dateOfAddString = dateOfAdd.ToString("dd.mm.yyyy HH:mm:ss");

        Console.WriteLine("Введите Ф.И.О сотрудника");
        string fullName = Console.ReadLine()!;

        int age = ReadInt("Введите возраст сотрудника");
        string ageString = age.ToString();

        int height = ReadInt("Введите рост сотрудника");
        string heightString = height.ToString();

        DateTime dateOfBirth = ReadDateTime();
        string dateOfBirthString = dateOfBirth.ToString("dd.mm.yyyy");

        Console.WriteLine("Введите место рождения сотрудника");
        string territory = Console.ReadLine()!;

        return $"{id}#{dateOfAddString}#{fullName}#{ageString}#{heightString}#{dateOfBirthString}#{territory}";
    }

    static DateTime ReadDateTime()
    {
        do
        {
            Console.WriteLine("Введите дату рождения сотрудника(dd.mm.yyyy)");
            if (DateTime.TryParseExact(Console.ReadLine()!, "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dateTime))
            {
                return dateTime;
            }
        } while (true);
    }

    static int ReadInt(string message)
    {
        do
        {
            Console.WriteLine(message);
            if (int.TryParse(Console.ReadLine()!, out int integer))
            {
                return integer;
            }
        } while (true);
    }

    static void SetData(string line)
    {
        using (StreamWriter sw = File.AppendText("data.txt"))
        {
            sw.WriteLine(line);
        }
    }

    static void ShowData()
    {
        if (!File.Exists("data.txt"))
        {
            Console.WriteLine("Файл еще не создан.");
        }
        else
        {
            using (StreamReader sr = new StreamReader("data.txt"))
            {
                string line;
 
                while ((line = sr.ReadLine()!) != null)
                {
                    string[] splitLine = line.Split("#");
                    foreach (var e in splitLine)
                    {
                        Console.Write($"{e} ");
                    }
                    Console.WriteLine();
                }
            }
        }
    }

    static int SetID()
    {
        if (File.Exists("data.txt"))
        {
            string lastLine = File.ReadLines("data.txt").Last();
            string idLastLine = lastLine.Split("#").First();
            int nextID = int.Parse(idLastLine) + 1;
            return nextID;
        }
        else
        {
            return 0;
        }
    }
}

